import random

def jogo_da_forca():
    palavras = ['python', 'programacao', 'computador', 'algoritmo', 'openai', 'inteligencia']
    palavra = random.choice(palavras)
    chances = 6
    letras_certas = []
    letras_erradas = []
    palavra_descoberta = '_' * len(palavra)

    while chances > 0 and palavra_descoberta != palavra:
        print("Palavra:", palavra_descoberta)
        print("Chances restantes:", chances)
        letra = input("Digite uma letra: ").lower()

        if letra in palavra:
            letras_certas.append(letra)
            indices = [i for i, l in enumerate(palavra) if l == letra]
            for i in indices:
                palavra_descoberta = palavra_descoberta[:i] + letra + palavra_descoberta[i+1:]
        else:
            letras_erradas.append(letra)
            chances -= 1
            print("Letras erradas:", letras_erradas)

    if palavra_descoberta == palavra:
        print("Parabéns, você ganhou! A palavra era:", palavra)
    else:
        print("Game over! A palavra era:", palavra)

jogo_da_forca()
