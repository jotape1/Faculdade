lista_inicial = [1, 2, 3, 4, 5]
lista_modificada = [2, 3, 5, 6, 7]

nao_mudaram = set(lista_inicial) & set(lista_modificada)
novos_elementos = set(lista_modificada) - set(lista_inicial)
removidos = set(lista_inicial) - set(lista_modificada)

print("Elementos que não mudaram:", nao_mudaram)
print("Novos elementos:", novos_elementos)
print("Elementos removidos:", removidos)
