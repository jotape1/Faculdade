lista1 = [1, 2, 3, 4, 5]
lista2 = [4, 5, 6, 7, 8]

comuns = set(lista1) & set(lista2)
apenas_lista1 = set(lista1) - set(lista2)
apenas_lista2 = set(lista2) - set(lista1)
uniao = set(lista1) | set(lista2)
lista1_sem_repetidos = list(set(lista1) - set(lista2))

print("Valores comuns:", comuns)
print("Valores apenas na primeira lista:", apenas_lista1)
print("Valores apenas na segunda lista:", apenas_lista2)
print("União das duas listas:", uniao)
print("Primeira lista sem elementos repetidos da segunda lista:", lista1_sem_repetidos)
