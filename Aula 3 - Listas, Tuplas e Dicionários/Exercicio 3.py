def verifica_parenteses(expressao):
    pilha = []

    for char in expressao:
        if char == '(':
            pilha.append(char)
        elif char == ')':
            if not pilha:
                return False
            pilha.pop()

    return not pilha

expressao1 = "(()(()()))"
expressao2 = "(()))(()"

print(verifica_parenteses(expressao1))  # Saída: True
print(verifica_parenteses(expressao2))  # Saída: False
