lugares_vagos = [10, 2, 1, 3, 0]

while True:
    sala = int(input("Número da sala (digite 0 para sair): "))
    if sala == 0:
        break
    lugares_solicitados = int(input("Quantidade de lugares solicitados: "))

    if lugares_vagos[sala - 1] >= lugares_solicitados:
        lugares_vagos[sala - 1] -= lugares_solicitados
        print("Lugares vendidos com sucesso!")
    else:
        print("Desculpe, não há lugares suficientes disponíveis.")

print("Programa encerrado.")
